from django.urls import reverse
from django.test import TestCase
from django.urls.exceptions import NoReverseMatch
from musiques.models import *
from .views import *

# création d'un morceau (enregistre et retourne) pour l'utilisation des tests
def creer_morceau():
    art = Artiste(nom="Imagins Dragons")
    art.save()
    res = Morceau(titre="Radioactive",date_sortie="2013-12-12", clefE=art)
    res.save()
    return res

# tests Morceaux
class MorceauxTests(TestCase):
    def test_liste(self):
        #verif l'affichage d'un morceau créée
        morceau = creer_morceau()
        reponse = self.client.get(reverse('musiques:url_base'))
        self.assertEqual(reponse.status_code, 200)
        self.assertContains(reponse, morceau.titre)
        self.assertQuerysetEqual(reponse.context['morceaux'], [repr(morceau)])

    def test_ajout_morceau(self):
        # verif la création d'un morceau
        art = Artiste(nom="Imagins Dragons")
        art.save()
        data = {
            'titre': 'Radioactive',
            'date_sortie': '2013-12-12',
            'clefE': art,
        }
        reponse = self.client.post(reverse('musiques:url_createM'), data)
        self.assertEqual(reponse.status_code, 200)

    #verif suppression morceau
    def test_morceau_delete(self):
        morceau = creer_morceau()
        aSuppr = Morceau.objects.get(titre='Radioactive')
        url = reverse('musiques:url_deleteM', args=[aSuppr.pk])
        response = self.client.get(url)
        assert response.status_code == 200

    #verif modification morceau
    # def test_morceau_update(self):
    #     morceau = creer_morceau()
    #     aModif = Morceau.objects.get(titre='Radioactive')
    #     url = reverse('musiques:url_updateM', args=[aModif.pk])
    #     response = self.client.get(url)
    #     aModif.titre="Modif"
    #     aModif.save()
    #     assert response.status_code == 200


#Même principe pour les tests classe Artiste
