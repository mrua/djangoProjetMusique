from django.shortcuts import render, get_object_or_404
from django .http import HttpResponse
from .models import *
from django.views.generic import DetailView, UpdateView, CreateView, DeleteView
from .form import *
from django.core.urlresolvers import reverse_lazy

class MorceauDetailView(DetailView):
    model = Morceau

class URLUpdate(UpdateView):
    model = Morceau
    template_name = 'musiques/Modif.html'
    form_class = MorceauForm
    success_url = '/musiques/base/'

class URLUpdateA(UpdateView):
    model = Artiste
    template_name = 'musiques/Modif.html'
    form_class = ArtisteForm
    success_url = '/musiques/base/'

class URLCreate(CreateView):
    model = Morceau
    template_name = 'musiques/Ajout.html'
    form_class = MorceauForm
    success_url = '/musiques/base/'

class URLCreateA(CreateView):
    model = Artiste
    template_name = 'musiques/Ajout.html'
    form_class = ArtisteForm
    success_url = '/musiques/base/'

class URLDelete(DeleteView):
    model = Morceau
    template_name = 'musiques/Suppr.html'
    context_object_name = "donnees"
    form_class = MorceauForm
    success_url = '/musiques/base/'
    def get_object(self, queryset=None):
        code = self.kwargs.get('pk', None)
        return get_object_or_404(Morceau, id=code)

class URLDeleteA(DeleteView):
    model = Artiste
    template_name = 'musiques/SupprA.html'
    context_object_name = "donnees"
    form_class = ArtisteForm
    success_url = '/musiques/base/'
    def get_object(self, queryset=None):
        code = self.kwargs.get('pk', None)
        return get_object_or_404(Artiste, id=code)


# Create your views here.
def morceau_detail(request, pk):
    return HttpResponse('OK')

def Base_View(request):
    morceaux = Morceau.objects.all()
    return render(request,"musiques/Base_View.html", {"morceaux":morceaux})

def artiste_View(request):
    artistes = Artiste.objects.all()
    return render(request,"musiques/Artistes.html", {"artistes":artistes})
