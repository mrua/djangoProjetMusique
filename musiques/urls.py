from django.conf.urls import url, include
from .views import *
from . import views
from django.views.generic import TemplateView

app_name = 'musiques'

urlpatterns = [
    url(r'^modifMorceau/(?P<pk>\d+)$', views.URLUpdate.as_view(), name='url_updateM'),
    url(r'^modifArtiste/(?P<pk>\d+)$', views.URLUpdateA.as_view(), name='url_updateA'),
    url(r'^SupprMorceau/(?P<pk>\d+)$', views.URLDelete.as_view(), name='url_deleteM'),
    url(r'^SupprArtiste/(?P<pk>\d+)$', views.URLDeleteA.as_view(), name='url_deleteA'),
    url(r'^ajoutMorceau/$', views.URLCreate.as_view(), name='url_createM'),
    url(r'^ajoutArtiste/$', views.URLCreateA.as_view(), name='url_createA'),
    url(r'^base/$', views.Base_View, name='url_base'),
    url(r'^Artistes/$', views.artiste_View, name='url_artistes'),
]
