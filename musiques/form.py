from django import forms
from .models import *


class MorceauForm(forms.ModelForm):
    class Meta:
        model = Morceau
        fields = '__all__'

class ArtisteForm(forms.ModelForm):
    class Meta:
        model = Artiste
        fields = '__all__'
