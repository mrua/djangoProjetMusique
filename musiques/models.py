from django.db import models

# Create your models here.
class Artiste(models.Model):
    nom = models.CharField(max_length=64)

    def __str__(self):
        return '{self.nom}'.format(self=self)

class Morceau(models.Model):
    # artiste = models.CharField(max_length=64)
    titre = models.CharField(max_length=64)
    date_sortie = models.DateField(null=True)
    clefE = models.ForeignKey(Artiste, null=True)

    def __str__(self):
        return '{self.titre}'.format(self=self)
